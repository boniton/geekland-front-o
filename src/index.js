import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './index.css';

// views
import Account from './views/Account';
import Cart from './views/Cart';
import Contact from './views/Contact';
import Home from './views/Home';
import Products from './views/Products';
import SignIn from './views/SignIn';
import SignUp from './views/SignUp';
import NoMatch from './views/NoMatch';
import ConditionsGéneral from './views/ConditionsGeneral';
import ModedePaiement from './views/ModedePaiement';
import ModeLivraison from './views/ModeLivraison';
import Cookies from './views/Cookies';
import PersonalInformation from './views/PersonalInformation';
import OrderHistory from './views/OrderHistory';
import Product from './views/Product';

// import test components
import WorkB from './work/WorkB';
import WorkJ from './work/WorkJ';
import WorkL from './work/WorkL';
import WorkO from './work/WorkO';

ReactDOM.render(
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<App />}>
                <Route index element={<Home />} />
                <Route path="account" element={<Account />}>
                    <Route path="info" element={<PersonalInformation />} />
                    <Route path="orders" element={<OrderHistory />} />
                </Route>
                <Route path="cart" element={<Cart />} />
                <Route path="contact" element={<Contact />} />
                <Route exact path="product/:id" element={<Product />} />
                <Route path="products" element={<Products />} />
                <Route exact path="products/:rubric" element={<Products />} />
                <Route path="signin" element={<SignIn />} />
                <Route path="signup" element={<SignUp />} />
                <Route path="modeledelivraison" element={<ModeLivraison />} />
                <Route path="modelepaiment" element={<ModedePaiement />} />
                <Route path="cgv" element={<ConditionsGéneral />} />
                <Route path="cookies" element={<Cookies />} />
                <Route path="*" element={<NoMatch />} />
            </Route>
            <Route path="/workb" element={<WorkB />} />
            <Route path="/workj" element={<WorkJ />} />
            <Route path="/workl" element={<WorkL />} />
            <Route path="/worko" element={<WorkO />} />
        </Routes>
    </BrowserRouter>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
