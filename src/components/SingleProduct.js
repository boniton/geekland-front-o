import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../css/SingleProduct.css';

const SingleProduct = (props) => {
    let navigate = useNavigate();

    return (
        <div
            className="single-product"
            onClick={() => navigate('/product/' + props.id)}
        >
            <div className="single-product-brand">{props.brand}</div>
            <div className="single-product-image">
                <img
                    src="/assets/images/produit1.jpg"
                    width="75%"
                    alt={props.model}
                />
            </div>
            <div className="single-product-bottom">
                <div>{props.model}</div>
                <div>{props.price} €</div>
            </div>
        </div>
    );
};

export default SingleProduct;
