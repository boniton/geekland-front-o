import React from 'react';
import '../css/Footer.css';
import { FaFacebook, FaInstagram } from 'react-icons/fa';
import { Link } from 'react-router-dom';

export default class Footer extends React.Component {
    render() {
        return (
            <div>
                <footer>
                    <ul className="footer-nav">
                        <li>
                            <Link to="cgv">Conditions génerales</Link>
                        </li>
                        <li>
                            <Link to="cookies">Cookies</Link>
                        </li>
                        <li>
                            <Link to="modeledelivraison">
                                Modes de livraison
                            </Link>
                        </li>
                        <li>
                            <Link to="modelepaiment">Moyens de paiement</Link>
                        </li>
                        <li>
                            <Link to="contact">Contact</Link>
                        </li>
                        <li>
                            <FaFacebook className="icone" />
                        </li>
                        <li>
                            <FaInstagram className="icone" />
                        </li>
                    </ul>
                </footer>
            </div>
        );
    }
}
