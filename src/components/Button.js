import React from 'react';
import '../css/Button.css';

const Button = (props) => {
    return (
        <div className="button">
            <input type="submit" value={props.value} onClick={(e) => { e.preventDefault() }} ></input>
        </div>
    );
}

export default Button;
