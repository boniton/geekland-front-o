import React from 'react';
import '../css/UserReviewForm.css';

const UserReviewForm = () => {
    return (
        <form id='user-review-form' onSubmit={(e) => e.preventDefault()}>
            <div>Donnez votre avis</div>
            <input id='user-review-area' type='textarea' rows='4' placeholder='Votre avis...'></input>
            <button>OK</button>
        </form>
    );
}

export default UserReviewForm;
