import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Config from '../config/index.js';
import SingleReview from './SingleReview';

const ReviewList = (props) => {
    let [list, setList] = useState([]);

    let url = Config.baseBackUrl;

    useEffect(() => {
        axios
            .get(url, { params: { rubric: 'reviewlist', id: props.id } })
            .then((response) => {
                let componentList = [];
                response.data.forEach((it) => {
                    componentList.push(
                        <SingleReview
                            key={it.id}
                            data={it}
                        />
                    );
                });

                setList(componentList);
            })
            .catch(function (error) {
                console.error(error);
            });
    }, [url, props.id]);

    return <div id="review-list">{list.length === 0 ? '' : list}</div>;
};

export default ReviewList;
