import React from 'react';
import '../css/ProductsFilter.css';
import { FaMouse, FaKeyboard } from 'react-icons/fa';
import { RiComputerLine, RiComputerFill } from 'react-icons/ri';

const ProductsFilter = (props) => {
    return (
        <div className="product-filter">
            <div className="product-filter-title">FILTRER LES PRODUITS</div>
            <div className='product-filter-filters'>
                <div className='product-filter-item'>
                    <RiComputerFill className='product-filter-icon' />
                    <div>Ordinateurs</div>
                </div>
                <div className='product-filter-item'>
                    <FaKeyboard className='product-filter-icon' />
                    <div>Claviers</div>
                </div>
                <div className='product-filter-item'>
                    <RiComputerLine className='product-filter-icon' />
                    <div>Écrans</div>
                </div>
                <div className='product-filter-item'>
                    <FaMouse className='product-filter-icon' />
                    <div>Souris</div>
                </div>
                {props.hasCutPrice ? <div>10% 20% 30%</div> : ''}
            </div>
        </div>
    );
}

export default ProductsFilter;
