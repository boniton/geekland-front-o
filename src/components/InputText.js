import React from 'react';

export default class InputText extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
       
    }
      
    handleChange(event) {   
       this.setState({value: event.target.value});
    }

    render() {
       
        return (
            <div className='input'>
                <input type='text' value={this.state.value} onChange={this.handleChange} placeholder={this.props.placeholder} >

                </input>
            </div>
        );
    }
}