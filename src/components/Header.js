import React from 'react';
import { FaHome, FaShoppingCart, FaUser } from 'react-icons/fa';
import { BiSearch } from 'react-icons/bi';
import { Link } from 'react-router-dom';
import '../css/Header.css';

export default class Header extends React.Component {
    render() {
        return (
            <div>
                <ul className="menu">
                    <li>
                        <Link to="/"><FaHome className='icone' /></Link>
                    </li>
                    <li>
                        <Link to="products">Produits</Link>
                    </li>
                    <li>
                        <Link to="products/news">Nouveautés</Link>
                    </li>
                    <li>
                        <Link to="products/promotions">Promotions</Link>
                    </li>
                    <li>
                        <div className="search-container">
                            <input type="text" placeholder='Rechercher un produit, une marque...'></input>
                            <BiSearch className="search-icon" />
                        </div>
                    </li>
                    <li>
                        <Link to="cart">
                            <FaShoppingCart className="icone" />
                        </Link>
                    </li>
                    <li>
                        <Link to="account">
                            <FaUser className="icone" />
                        </Link>
                    </li>
                </ul>
            </div>
        );
    }
}
