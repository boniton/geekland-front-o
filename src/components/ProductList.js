import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import SingleProduct from './SingleProduct';
import '../css/ProductList.css';
import Config from '../config/index.js';

const ProductList = (props) => {
    let { rubric } = useParams();
    let [list, setList] = useState([]);

    let url = Config.baseBackUrl;
    let checkedRubric = rubric === undefined ? 'all' : rubric;

    useEffect(() => {
        axios
            .get(url, { params: { rubric: checkedRubric } })
            .then((response) => {
                // handle success
                let componentList = [];
                response.data.forEach((element) => {
                    componentList.push(
                        <SingleProduct
                            key={element.id}
                            id={element.id}
                            name={element.name}
                            brand={element.brand}
                            model={element.model}
                            price={element.price}
                        />
                    );
                });

                setList(componentList);
            })
            .catch(function (error) {
                // handle error
                console.error(error);
            });
    }, [checkedRubric, url]);

    return <div id="product-list">{list.length === 0 ? '' : list}</div>;
};

export default ProductList;
