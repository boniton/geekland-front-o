import React from 'react';
import '../css/CartProduct.css';
import { MdCancel } from 'react-icons/md';

export default class CartPoduct extends React.Component {
    render() {
        return (
            <div className="cart">
                <div id="name">{this.props.name}</div>
                <div id="brand">{this.props.brand}</div>
                <div id="price">{this.props.price}</div>
                <div id="cancel">
                    <MdCancel />
                </div>
            </div>
        );
    }
}
