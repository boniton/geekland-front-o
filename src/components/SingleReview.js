import React from 'react';
import '../css/SingleReview.css';

const SingleReview = (props) => {

    let item = props.data;

    return (
        <div className='single-review'>
            <div className='single-review-date'>{item.date}</div>
            <div className='single-review-name'>{item.name}</div>
            <div className='single-review-opinion'>{item.opinion}</div>
        </div>
    );
}

export default SingleReview;
