import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Config from '../config/index.js';

const ProductDetails = (props) => {
    let [item, setItem] = useState({});

    let url = Config.baseBackUrl;

    useEffect(() => {
        axios
            .get(url, { params: { rubric: 'product', id: props.id } })
            .then((response) => {
                setItem(response.data[0]);
            })
            .catch(function (error) {
                console.error(error);
            });
    }, [url, props.id]);

    if (item === undefined) return <div>???</div>;

    return (
        <>
            <div>{item.brand}</div>
            <div>{item.category_id}</div>
            <div>{item.cut_price_percentage}</div>
            <div>{item.description}</div>
            <div>{item.id}</div>
            <div>{item.model}</div>
            <div>{item.name}</div>
            <div>{item.price}</div>
            <div>{item.record_date}</div>
        </>
    );
};

export default ProductDetails;
