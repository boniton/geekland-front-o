import React from 'react';
import Button from '../components/Button';
import CartPoduct from '../components/CartProduct';
import '../css/Cart.css';

export default class Cart extends React.Component {
    render() {
        return (
            <div id="cart">
                <div id="cartproduct">
                    <CartPoduct brand="Lenovo" name="LX218" price="100€" />
                    <CartPoduct brand="Lenovo" name="LX218" price="100€" />
                    <CartPoduct brand="Lenovo" name="LX218" price="100€" />
                </div>
                <div>
                    <div id="total">
                        <h2>TOTAL</h2>
                    </div>

                    <div id="payer">
                        <Button value="PAYER" />
                    </div>
                </div>
            </div>
        );
    }
}
