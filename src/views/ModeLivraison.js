import React from 'react';

export default class ModeLivraison extends React.Component {
    render() {
        return (
            <div>
                <h1>MODES DE LIVRAISON</h1>

                <h3>chronopost</h3>
                <p>
                    CHRONOPOST / CHRONOPOST EXPRESS Chronopost est disponible
                    pour tous les articles vendus et expédiés par LDLC.com pour
                    les livraisons en France (DOM-TOM compris, à quelques
                    exceptions près pour les TOM), Belgique et Suisse. Nous
                    utilisons également les services de Chronopost par défaut
                    pour toutes les expéditions pour le reste du monde.
                </p>

                <h3>livraison en boutique</h3>
                <p>
                    Comment se déroule ma commande ? L'expédition est réalisée
                    depuis l'un de nos centres logistiques. Vous serez informé
                    par email de l'arrivée de la commande en boutique. Lorsque
                    tous les produits de la commande sont disponibles, 3 à 4
                    jours sont généralement nécessaires entre la validation
                    d'une commande et sa mise à disposition en boutique.
                </p>

                <h3>Relais Colis</h3>
                <p>
                    Faites-vous livrer les articles vendus et expédiés par LDLC
                    près de chez vous, et disposez d'une large plage horaire
                    pour prendre possession de vos colis ! Relais colis, avec
                    ses 4000 relais sur la France métropolitaine, permet de se
                    faire livrer près de chez soi pour un coût d'expédition très
                    bas.
                </p>
            </div>
        );
    }
}
