import React from 'react';
import '../css/Cgv.css';

export default class ConditionsGeneral extends React.Component {
    render() {
        return (
            <div className="para">
                <h1>CONDITIONS GENERALES DE VENTE – INTERNET</h1>
                <h4> ARTICLE 1 - Champ d'application </h4>
                <p>
                    Les présentes conditions générales de vente s'appliquent à
                    toutes commandes passées sur ou avec le site internet
                    www.boulanger.com, via la solution de tchat disponible sur
                    le site www.boulanger.com ou par téléphone via le 3011
                    depuis la France ou le 0 800 30 30 11 depuis l'étranger,
                    numéros disponibles 7J/7 de 8h à 22h (Numéros gratuits), à
                    l’exclusion des produits vendus sur la marketplace pour
                    lesquels ce sont les Conditions Générales de Vente
                    Partenaires qui s’appliquent. Les présentes conditions
                    s'appliquent à tout consommateur tel que défini par les
                    directives européennes, à savoir « toute personne physique
                    qui agit à des fins qui n’entrent pas dans le cadre de son
                    activité commerciale, industrielle, artisanale ou libérale
                    », et s’appliquent à l'exclusion de toutes autres
                    conditions, notamment celles en vigueur pour les ventes en
                    magasin. Dans l'intégralité des présentes, le client
                    (ci-après « Client ») est entendu comme celui avec lequel
                    s'établit la relation d'achat-vente et lequel est facturé au
                    titre de la commande.
                </p>
                <h4> ARTICLE 2 - PRIX </h4>
                <p>
                    2.1. Tous les prix des produits et services proposés à la
                    vente sur le site www.boulanger.com sont exprimés en euros
                    toutes taxes comprises hors participation aux frais de
                    traitement et d'expédition. Ces frais de traitement et
                    d'expédition sont ci-après consultables.
                </p>
                <p>
                    2.2. Boulanger se réserve la faculté de modifier ses prix à
                    tout moment mais s'engage à appliquer les tarifs en vigueur
                    lors de l'enregistrement de la commande sous réserve de la
                    disponibilité des produits à cette date.
                </p>
                <p>
                    2.3. Les produits demeurent la propriété de Boulanger
                    jusqu'à plein et entier encaissement du prix. Toutefois à
                    compter de l'enlèvement ou de la réception de la commande
                    par le Client ou son représentant, les risques des
                    marchandises retirées ou livrées sont transférés au C
                </p>
                <h4>ARTICLE 3-DISPONIBILITE</h4>
                Les offres de produits et de prix sont valables tant qu'elles
                sont visibles sur le site, dans la limite des stocks
                disponibles.
            </div>
        );
    }
}
