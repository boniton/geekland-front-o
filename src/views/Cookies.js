import React from 'react';

export default class Cookies extends React.Component {
    render() {
        return (
            <div>
                <h1>A propos des cookies et traceurs</h1>
                <h3> Vos données, votre choix</h3>
                <p>
                    Sur nos sites et nos applications, nous recueillons à
                    chacune de vos sites des données vous concernant. Ces
                    données nous permettent de vous proposer les offres et
                    services les plus pertinents pour vous, et de vous adresser,
                    en direct ou via des partenaires, des communications et
                    publicités personnalisées et de mesurer leur efficacité. Ces
                    données nous permettent également d’adapter le contenu de
                    nos sites à vos préférences, de vous faciliter le partage de
                    contenu sur les réseaux sociaux et de réaliser des
                    statistiques. L’opposition aux cookies et autres traceurs se
                    fait par le dépôt d’un cookie. Par conséquent, l’opposition
                    ne vaut que pour l’appareil sur lequel vous vous opposez. Si
                    vous vous connectez avec un autre appareil, ou si vous
                    supprimez les cookies de votre appareil, vous devrez de
                    nouveau régler vos paramètres de consentements. Les cookies
                    ont une durée de validité de 13 mois maximum. Les
                    identifiants digitaux associés aux cookies sont conservés 13
                    mois à l’exception de l’identifiant Google analytics qui est
                    conservé 14 mois.
                </p>
                <h3> Les données collectées</h3>
                <p>
                    Lorsque vous naviguez sur internet, les traceurs utilisent
                    un identifiant digital (ID cookie ou ID mobile) qui permet
                    de vous ré identifier lors d’une visite ultérieure. Selon le
                    type de traitement et le type de traceur, des données
                    complémentaires comme par exemple des données de navigation
                    (pages visitées, produits consultés, clics effectués),
                    l’adresse IP, le N° de commande etc. peuvent être associées
                    à votre identifiant digital. Les données complémentaires
                    dépendent des traitements, cette information sera précisée
                    le cas échéant au niveau de chaque rubrique cookie et autres
                    traceurs. Les différents émmetteurs Les cookies Il s'agit
                    des cookies déposés par Boulanger sur votre terminal. Il
                    peut s’agir d’un cookie spécifique ou d’un cookie nécessaire
                    à une fonctionnalité opérée par la solution logicielle d’un
                    de nos sous-traitants (exemple le Tchat). Les cookies tiers
                    Il s'agit des cookies déposés par d’autres sociétés que
                    Boulanger sur votre terminal (par exemple des régies
                    publicitaires, des partenaires, d’autres responsables de
                    traitements).
                    <br />
                    Plus d'informations
                    <br />
                    Retrouvez plus d'informations sur l'utilisation des cookies
                    sur le site de la CNIL :
                    http://www.cnil.fr/vos-droits/vos-traces/les-cookies/
                </p>
            </div>
        );
    }
}
