import React from 'react';
import Button from '../components/Button';
import InputText from '../components/InputText';
import '../css/Form.css';

export default class SignUp extends React.Component {
    render() {
        return (
            <div>
                <form className="form">
                    <h1> Créer un Compte</h1>
                    <InputText placeholder=" Votre nom" />
                    <InputText placeholder=" Votre prénom" />
                    <input type="email" placeholder="E-mail" />
                    <input type="tel" placeholder="Téléphone" />
                    <Button value="créer" />
                </form>
            </div>
        );
    }
}
