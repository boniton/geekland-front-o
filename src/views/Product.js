import React from 'react';
import { useParams } from 'react-router-dom';
import ProductDetails from '../components/ProductDetails';
import UserReviewForm from '../components/UserReviewForm';
import ReviewList from '../components/ReviewList';

const Product = () => {
    let { id } = useParams();

    return (
        <div>
            <ProductDetails id={id} />
            <UserReviewForm id={id} />
            <ReviewList id={id} />
        </div>
    );
};

export default Product;
