import React from 'react';
import Button from '../components/Button';
import InputText from '../components/InputText';
import '../css/Form.css';

export default class Contact extends React.Component {
    render() {
        return (
            <div>
                <form className="form">
                    <div>
                        <h1> Contactez-nous</h1>
                    </div>
                    <InputText placeholder="nom" />
                    <InputText placeholder="prénom" />
                    <input type="email" placeholder="E-email" />
                    <input type="tel" placeholder="Télephone" />
                    <textarea defaultValue="Message"></textarea>
                    <Button value="Envoyer" />
                </form>
            </div>
        );
    }
}
