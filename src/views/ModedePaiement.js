import React from 'react';

export default class ModeLivraison extends React.Component {
    render() {
        return (
            <div>
                <h1>Mode de paiement</h1>
                <h2>Carte Banquaire</h2>
                <p>
                    La carte bancaire est le moyen de paiement le plus utilisé
                    par notre clientèle. C'est aussi la seule méthode de
                    règlement qui vous sera proposée lors de commandes passées
                    auprès de nos partenaires-vendeurs sur la marketplace LDLC.
                    Pour tous vos paiements par carte bancaire, la transaction
                    est sécurisée par la technologie de cryptage SSL. Toutes vos
                    coordonnées relatives au paiement sont cryptées depuis votre
                    ordinateur jusqu'au serveur de la banque. Ces informations
                    restent confidentielles et ne peuvent être vues ou utilisées
                    à votre insu.
                </p>
                <h2>Paiement en trois Fois</h2>
                <p>
                    Le paiement en 3 fois par carte bancaire est disponible pour
                    toute commande comprise entre 100 € et 2000 €, valable
                    exclusivement sur les articles vendus et expédiés par LDLC
                    et hors livraison en boutique LDLC. La date de validité de
                    la carte bancaire doit être au minimum de 4 mois après la
                    date d'achat, 6 mois si l'un des articles de votre commande
                    n'est pas disponible immédiatement. Pour des raisons
                    techniques, les cartes virtuelles (E-Card), les cartes
                    bancaires prépayées ou les cartes Electron ne peuvent pas
                    être sélectionnées pour un règlement en 3 fois.
                </p>
                <h2> Virement bancaire</h2>
                <p>
                    Nous acceptons le règlement par virement pour toute commande
                    d'articles vendus et expédiés par LDLC (hors article
                    Marketplace et hors livraison Relais). Merci de noter que le
                    matériel ne pourra être réservé avant la réception du
                    paiement. Si vous souhaitez être livré le plus rapidement
                    possible et alors faire réserver le matériel en stock, nous
                    vous conseillons un règlement par carte bancaire.
                </p>
            </div>
        );
    }
}
