import React from 'react';
import Button from '../components/Button';
import '../css/Form.css';

export default class SignIn extends React.Component {
    render() {
        return (
            <div>
                <form className="form">
                    <h1>Identifiez-vous</h1>
                    <input type="email" placeholder="votre email" />
                    <input type="password" />
                    <Button value="connexion " />
                    <div> Nouveau client?</div>
                    <Button value="créer un compte " />
                </form>
            </div>
        );
    }
}
