import React, { useState, useEffect } from 'react';
import axios from 'axios';

// import Swiper core and required modules
import { Navigation, Pagination, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

// import project
import Config from '../config';
import SingleProduct from '../components/SingleProduct';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import '../css/Swiper.css';

const Home = () => {

    let [newsList, setNewsList] = useState([]);
    let [promoList, setPromoList] = useState([]);

    // get products promotions
    useEffect(() => {
        console.log('ok');
        axios
            .get(Config.baseBackUrl, { params: { rubric: 'carousel-promotions' } })
            .then((response) => {                
                let list = [];
                let slideKey = 0;
                response.data.forEach((it, index) => {
                    list.push(
                        <SwiperSlide key={slideKey}>
                            <SingleProduct
                                key={index}
                                id={it.id}
                                name={it.name}
                                brand={it.brand}
                                model={it.model}
                                price={it.price}
                            />
                        </SwiperSlide>
                    );
                    slideKey++;
                });

                setPromoList(list);
            })
            .catch(function (error) {
                console.error(error);
            });


        axios
            .get(Config.baseBackUrl, { params: { rubric: 'carousel-news' } })
            .then((response) => {
                let list = [];
                let slideKey = 100;
                response.data.forEach((it, index) => {
                    list.push(
                        <SwiperSlide key={slideKey}>
                            <SingleProduct
                                key={index}
                                id={it.id}
                                name={it.name}
                                brand={it.brand}
                                model={it.model}
                                price={it.price}
                            />
                        </SwiperSlide>
                    );
                    slideKey++;
                });

                setNewsList(list);
            })
            .catch(function (error) {
                console.error(error);
            });

    }, []);


    // render component
    return (
        <>
            <div className='swiper-div'>
                <div className='swiper-title'>PROMOTIONS</div>
                <Swiper
                    key={0}
                    initialSlide={0}
                    modules={[Navigation, Pagination, A11y]}
                    spaceBetween={25}
                    slidesPerView={3}
                    slidesPerGroup={3}
                    loop={true}
                    loopFillGroupWithBlank={true}
                    navigation={true}
                    pagination={{ clickable: true }}
                >
                    {promoList}
                </Swiper>
            </div>

            <div className='swiper-div'>
                <div className='swiper-title'>NOUVEAUTÉS</div>
                <Swiper
                    key={1}
                    modules={[Navigation, Pagination, A11y]}
                    spaceBetween={25}
                    slidesPerView={3}
                    slidesPerGroup={3}
                    loop={true}
                    loopFillGroupWithBlank={true}
                    navigation={true}
                    pagination={{ clickable: true }}
                >
                    {newsList}
                </Swiper>
            </div>
        </>
    );
}

export default Home;
