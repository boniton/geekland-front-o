import React from 'react';
import { Link, Outlet } from 'react-router-dom';
import '../views/PersonalInformation';
import '../css/Account.css';

const Account = () => {
    return (
        <div className="account">
            <div id="accountmenu">
                <ul>
                    <li>
                        <h2>Mon compte</h2>
                    </li>
                    <li>
                        <Link to="info">Mes informations</Link>
                    </li>
                    <li>
                        <Link to="orders">Mes commandes</Link>
                    </li>
                </ul>
            </div>
            <div id="accountoutlet">
                <Outlet />
            </div>
        </div>
    );
}

export default Account;
