import React from 'react';
import ProductList from '../components/ProductList';
import ProductsFilter from '../components/ProductsFilter';
import '../css/Products.css';

export default class Poducts extends React.Component {
    render() {
        return (
            <div className="products">
                <ProductsFilter />
                <ProductList rubric={this.props.rubric} />
            </div>
        );
    }
}
