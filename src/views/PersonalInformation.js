import React from 'react';
import Button from '../components/Button';
import InputText from '../components/InputText';
import '../css/PersonalInformation.css';

const PersonalInformation = () => {
    return (
        <div>
            <form className="personal-info-form">
                <div>
                    <h3>Mes coordonnées</h3>
                </div>

                <div className='personal-info-genre'>
                    <input id='miss' type="radio" name='genre' />
                    <label htmlFor='miss'>Mme</label>

                    <input id='mister' type="radio" name='genre' />
                    <label htmlFor="mister">M.</label>
                </div>

                <InputText placeholder="Nom..." />
                <InputText placeholder="Prénom..." />

                <input type="email" placeholder="Mail..." />
                <input type="date" />
                <Button value="Modifier" />
            </form>
        </div>
    );
}

export default PersonalInformation;
