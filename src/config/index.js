export default class Config {
    static baseFrontUrl = process.env.NODE_ENV === 'production' ? 'https://g1.afci.dev/geekland-back-o/' : 'http://127.0.0.1/geekland-front-o/';
    static baseBackUrl = process.env.NODE_ENV === 'production' ? 'https://g1.afci.dev/geekland-o/' : 'http://127.0.0.1/geekland-back-o/';
}
